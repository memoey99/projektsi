﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            if (!Directory.Exists("./wykresy"))
            {
                Directory.CreateDirectory("./wykresy");
            }
            string s1 = File.ReadAllText("a.json");
            string s2 = File.ReadAllText("b.json");
            var d1 = JsonSerializer.Deserialize<List<float>>(s1);
            var d2 = JsonSerializer.Deserialize<List<int>>(s2);
            Chart chart1 = new Chart();
            chart1.Titles.Add("Funkcja przystosowania");
            
            Chart chart2 = new Chart();
            chart2.Titles.Add("Ilość śmierci");
            var series1 = new System.Windows.Forms.DataVisualization.Charting.Series
            {
                Name = "Series1",
                
                Color = System.Drawing.Color.Green,
                
                IsVisibleInLegend = false,
                IsXValueIndexed = true,
                ChartType = SeriesChartType.Line,
                MarkerStyle = MarkerStyle.Circle
            };
            var series2 = new System.Windows.Forms.DataVisualization.Charting.Series
            {
                Name = "Series2",
                Color = System.Drawing.Color.Green,
                IsVisibleInLegend = false,
                IsXValueIndexed = true,
                ChartType = SeriesChartType.Line,
                MarkerStyle = MarkerStyle.Circle
            };

            for (int i = 0; i < d1.Count; i++)
            {
                series1.Points.AddXY(i+1,d1[i] );
                series2.Points.AddXY(i+1,d2[i] );
            }
            chart1.Invalidate();
           
            ChartArea chartArea1 = new ChartArea();
            ChartArea chartArea2 = new ChartArea();
            chartArea1.AxisX.Title = "Numer pokolenia";
            
            chartArea1.AxisY.Title = "Średnia wartość funkcji przystosowania";
            chartArea1.Name = "ChartArea1";
            chart1.Size = new Size(800, 600);
            chart2.Size = new Size(800, 600);
            chartArea1.AxisX.MajorGrid.Enabled = false;
            chartArea1.AxisY.MajorGrid.Enabled = false;
            
            chartArea2.Name = "ChartArea2";
            chartArea2.AxisX.MajorGrid.Enabled = false;
            chartArea2.AxisY.MajorGrid.Enabled = false;
            chartArea2.AxisX.Title = "Numer pokolenia";
            chartArea2.AxisY.Title = "Liczba śmierci w pokoleniu";
            chart1.ChartAreas.Add(chartArea1);
            chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            chart1.Location = new System.Drawing.Point(0, 50);
            chart1.Name = "Fitness";
            chart1.Series.Add(series1);
            chart1.SaveImage($"wykresy/fitnessy {DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss")}.png", ChartImageFormat.Png);
            chart2.ChartAreas.Add(chartArea2);
            chart2.Dock = System.Windows.Forms.DockStyle.Fill;
            chart2.Location = new System.Drawing.Point(0, 50);
            chart2.Name = "Liczba smierci";
            chart2.Series.Add(series2);
            chart2.SaveImage($"wykresy/smierci - {DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss")}.png",ChartImageFormat.Png);
            
        }
    }
}
