using System;
using Godot;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Diagnostics;
using System.Globalization;
using CsvHelper;
using System.Linq;

public static class Genetic
{
	public class Osobnik
	{
		public double output1 = 0.0; //jumpForce
		public double output2 = 0.0; //miejsce skoku, odległośc od przeszkody
		public double fitness = 0.0;
		public double miejsceladowania = 0.0; //czy to jest uzywane?
		public double dolnaGranica = 0.0;
		public double gornaGranica = 1.0;
	}
	public static bool initialized = false;
	public static List<Osobnik> osobniki;   
	static int minJumpForce = 512; 
	static int maxJumpForce = 1023; 
	static int minWhen = 64; 
	static int maxWhen = 511; 
	public static List<float> fitnessy = new List<float>();
	public static List<int> deadcounty = new List<int>();
	public static void Diagramy()
	{
		var d1 = JsonSerializer.Serialize<List<float>>(fitnessy);
		var d2 = JsonSerializer.Serialize<List<int>>(deadcounty);
		System.IO.File.WriteAllText("a.json", d1);
		System.IO.File.WriteAllText("b.json", d2);
		try
		{

			Process firstProc = new Process();
			firstProc.StartInfo.FileName = ".\\apka\\ConsoleApp1.exe";
			firstProc.EnableRaisingEvents = true;

			firstProc.Start();

			firstProc.WaitForExit();

			
		}
		catch (Exception ex)
		{
			Console.WriteLine("An error occurred!!!: " + ex.Message);
			return;
		}
	}
	public static void Randomize(int ilosc)
	{
		osobniki = new List<Osobnik>();
		try
		{
			System.IO.File.Delete("./logi.txt");
		}
		catch (Exception e)
		{
			GD.Print("File error!");
		}
		Random random = new Random();
		for (int i = 0; i < ilosc; i++)
		{
			osobniki.Add(new Osobnik()
			{
				output1 = random.Next(minJumpForce, maxJumpForce),
				output2 = random.Next(minWhen, maxWhen)
			});
		}
	}
	public static List<Osobnik> GA()
	{
		Random random = new Random();
		
		double crossover = Parameters.pCrossover;
		double mutation = Parameters.pMutation;
		int elitism = Parameters.elitism;
		List<Osobnik> noweOsobniki = new List<Osobnik>();
		osobniki.Sort((x, y) => x.fitness < y.fitness ? -1 : 1);
		//tworzenie
		for (int j = 0; j < elitism; j++)
		{
			noweOsobniki.Add(osobniki[j]);
			osobniki[j].fitness = 0.0;
		}
		Ranking();
		int i = elitism;
		do
		{
			if (noweOsobniki.Count == Parameters.popSize) break;
			if (noweOsobniki.Count == (osobniki.Count - 1)) break;
			Osobnik[] rodzice = wybierzRodzica();
			List<Osobnik> dzieci = Crossover(rodzice[0], rodzice[1], crossover);
			dzieci = Mutation(dzieci, mutation);
			//mutacjaa
			
			noweOsobniki.Add(dzieci[0]);
			noweOsobniki.Add(dzieci[1]);
			i += 2;
		} while (i < osobniki.Count);

		if (noweOsobniki.Count < osobniki.Count)
		{
			int a = random.Next(elitism - 1, osobniki.Count - 1);
			noweOsobniki.Add(new Osobnik()
			{
				output1 = osobniki[a].output1,
				output2 = osobniki[a].output2
			});//tu losowy z listy
			/* noweOsobniki.Add(new Osobnik()
			 {
				 output1 = random.Next(minJumpForce, maxJumpForce),
				 output2 = random.Next(minWhen, maxWhen)
			 });*/
		}
		return noweOsobniki;
	}
	public static List<Osobnik> GetList()
	{
		return osobniki;
	}
	public static Osobnik GetOsobnikByIndex(int i)
	{
		return osobniki[i];
	}
	public static string PrintOsobnik(int i)
	{
		Osobnik o = osobniki[i];
		return ("Sila skoku: " + o.output1 + "  Miejsce skoku: " + o.output2);
	}
	public static string PrintRanking()
	{
		string print = "";
		int i = 1;
		foreach (Osobnik o in osobniki)
		{
			print += ("Osobnik " + i + "    s: " + o.output1 + "    m: " + o.output2 + "    fitness:" + Math.Round(o.fitness, 3) + "\n");
			i++;
		}
		return print;
	}
	public static void writeToFile(int numerpokolenia, int iloscsmierci, float srednifitness)
	{
		using (StreamWriter sw = System.IO.File.AppendText("./logi.txt"))
		{
			sw.WriteLine($"Pokolenie {numerpokolenia}");
			foreach (Osobnik o in osobniki)
			{
				sw.WriteLine("s: " + o.output1 + "    m: " + o.output2 + "    fitness:" + Math.Round(o.fitness, 3));
			}
			sw.WriteLine($"Ilosc smierci {iloscsmierci}");
			sw.WriteLine($"Sredni fitness {srednifitness}");
			sw.Close();
		}
	}
	public static void writeToFileCSV()
	{
		var records = new List<object>();
		for (int i = 0; i < fitnessy.Count; i++)
		{
			records.Add(new
			{
				Numer_pokolenia = i + 1,
				Fitness = fitnessy[i],
				Liczba_smierci = deadcounty[i]
			});
		}

		using (var writer = new StreamWriter($"./logi - {DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss")}.csv"))
		using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
		{
			csv.Configuration.Delimiter = ";";
			csv.WriteRecords(records);
		}
	}
	public static string Srednia()
	{
		double srednia = 0;
		foreach (Osobnik o in osobniki)
		{
			srednia += o.fitness;
		}
		return Math.Round((srednia / osobniki.Count), 3).ToString();
	}
	private static List<Osobnik> Mutation(List<Osobnik> dzieci, double mutation)
	{
		Random random = new Random();
		double p = random.NextDouble();
		if (p < mutation)
		{
			GD.Print("Mutacja1!");
			string chromosom = IntToBits((int)dzieci[0].output1, (int)dzieci[0].output2);
			int k = random.Next(0, 18);
			StringBuilder chr = new StringBuilder(chromosom);
			chr[k] = chromosom[k] == '0' ? '1' : '0';
			chromosom = chr.ToString();
			var tab = BitsToInt(chromosom);
			dzieci[0].output1 = tab[0];
			dzieci[0].output2 = tab[1];

		}
		p = random.NextDouble();
		if (p < mutation)
		{
			GD.Print("Mutacja2!");
			string chromosom = IntToBits((int)dzieci[1].output1, (int)dzieci[1].output2);
			int k = random.Next(0, 18);
			StringBuilder chr = new StringBuilder(chromosom);
			chr[k] = chromosom[k] == '0' ? '1' : '0';
			chromosom = chr.ToString();
			var tab = BitsToInt(chromosom);
			dzieci[1].output1 = tab[0];
			dzieci[1].output2 = tab[1];
		}
		return dzieci;
	}

	private static List<Osobnik> Crossover(Osobnik osobnik1, Osobnik osobnik2, double crossover)
	{
		List<Osobnik> dzieci = new List<Osobnik>();
		double p = new Random().NextDouble();
		if (p > crossover)
		{
			dzieci.Add(osobnik1);
			dzieci.Add(osobnik2);
		}
		else
		{
			//dziel chromosomy
			GD.Print("Krzyżowanie!");
			string chromosom1 = IntToBits((int)osobnik1.output1,(int)osobnik1.output2);
			string chromosom2 = IntToBits((int)osobnik2.output1,(int)osobnik2.output2);
			int k = new Random().Next(1,18);
			GD.Print("Miejsce podziału : " + k); 
			string nowy1 = chromosom1.Substring(0, k)+ chromosom2.Substring(k, 19 - k);
			string nowy2 = chromosom2.Substring(0, k) + chromosom1.Substring(k, 19 - k);
			int[] param1 = BitsToInt(nowy1);
			int[] param2 = BitsToInt(nowy2);

			dzieci.Add(new Osobnik()
			{
				output1 = param1[0],
				output2 = param1[1]
			}) ;
			dzieci.Add(new Osobnik()
			{
				output1 = param2[0],
				output2 = param2[1]
			});
		}
		return dzieci;
	}

	private static Osobnik[] wybierzRodzica()
	{
		Osobnik[] rodzice = new Osobnik[2];
		Random random = new Random();
		double p = random.NextDouble();

		Osobnik temp = osobniki.Find(x => (x.dolnaGranica <= p) && (x.gornaGranica >= p));
		rodzice[0] = (new Osobnik()
		{
			output1 = temp.output1,
			output2 = temp.output2
		});
		Osobnik temp2 = osobniki.Find(x => (x.dolnaGranica <= p) && (x.gornaGranica >= p) && x != temp);
		while (temp2 == null)
		{
			p = random.NextDouble();
			temp2 = osobniki.Find(x => (x.dolnaGranica <= p) && (x.gornaGranica >= p) && x != temp);
		}
		rodzice[1] = (new Osobnik()
		{
			output1 = temp2.output1,
			output2 = temp2.output2
		});
		return rodzice;
	}
	private static void Ranking()
	{
		double temp = 0.0;
		double sumaRang = (((1 + osobniki.Count) / 2.0) * osobniki.Count);
		double p = 0.0;
		for (int i = 0; i < osobniki.Count; i++)
		{
			//(-ranga+N+1)/suma_rang
			p = (osobniki.Count + 1 - (i + 1)) / sumaRang;
			osobniki[i].dolnaGranica = temp;
			osobniki[i].gornaGranica = temp + p;
			temp += p;
		}
	}

	public static string IntToBits(int in1, int in2)
	{
		string out1 = Convert.ToString(in1, 2);
		string out2 = Convert.ToString(in2, 2);
		string out3 = ("");
		for (int i = out1.Length; i < 10; i++)
		{
			out3 += '0';
		}
		out3 += out1;
		for (int i = out2.Length; i < 9; i++)
		{
			out3 += '0';
		}
		return (out3+out2);
	}
	
	public static int[] BitsToInt(string input)
	{
		String result = Convert.ToString(Convert.ToInt32(input.Substring(0, 10), 2), 10);

		int out1 = Convert.ToInt32(result);

		result = Convert.ToString(Convert.ToInt32(input.Substring(10, 9), 2), 10);

		int out2 = Convert.ToInt32(result);

		return new int[] {out1, out2};
	}
	
		
}
