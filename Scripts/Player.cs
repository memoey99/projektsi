using Godot;
using System;
using System.Collections.Generic;
public class Player : KinematicBody2D
{
	/* MOVEMENT */
	Vector2 motion = new Vector2();
	double gravity = Parameters.gravity;
	double jumpForce = 0.0;
	double whenJump = 0.0;
	/* OBSTACLES */
	StaticBody2D first;
	StaticBody2D second;
	int distance;
	int distance2;
	int best;
	float fitness;
	int generationCount;
	bool check = false;
	bool isSaved = false;
	public static int index = 0;
	public static int generation = 1;
	public static int deathCount = 0;
	public static string srednia = "";
	public static bool entered = false;
	public override void _Ready()
	{
		generationCount = Parameters.popSize;
		entered = false;
		var summary = GetNode("/root/Game/SummaryText") as TextEdit;
		var current = GetNode("/root/Game/CurrentText") as TextEdit;
		var last = GetNode("/root/Game/LastText") as TextEdit;
		first = GetNode("/root/Game/Obstacle1") as StaticBody2D;
		second = GetNode("/root/Game/Obstacle2") as StaticBody2D;

		best = (int)((second.Position.x - first.Position.x) / 2.0f); // środek między przeszkodami

		if (!Genetic.initialized)
		{
			Init();
			Genetic.Randomize(generationCount);
			Genetic.initialized = true;
		}
		if (index == generationCount)
		{
			Genetic.writeToFile(generation, deathCount, float.Parse(Genetic.Srednia()));
			Genetic.fitnessy.Add(float.Parse(Genetic.Srednia()));
			Genetic.deadcounty.Add(deathCount);
			deathCount = 0;
			index = 0;
			generation++;
			srednia = Genetic.Srednia();
			Genetic.osobniki = Genetic.GA();
			summary.Text = (Genetic.PrintRanking() + "\nLicznik smierci: " + deathCount);
		}

		jumpForce = Genetic.GetOsobnikByIndex(index).output1;
		whenJump = Genetic.GetOsobnikByIndex(index++).output2;

		summary.Text = ("Licznik smierci: " + deathCount + "\n" + Genetic.PrintRanking());
		last.Text = ("Srednia przystosowania poprzedniego pokolenia: " + srednia);
		current.Text = ("Pokolenie: " + generation + "/" + Parameters.maxiter +"\nOsobnik: " + index + "\n" + Genetic.PrintOsobnik(index - 1));
	}
	public override void _Process(float delta)
	{
		if (Input.IsActionJustPressed("exit"))
		{
			Genetic.Diagramy();
			Genetic.writeToFileCSV();
			Genetic.initialized = false;
			GetTree().ChangeScene("res://Scenes/Menu.tscn");
		}
		distance = (int)(first.Position.x - Position.x);
		distance2 = (int)(second.Position.x - Position.x);
	}
	public void OnEntered(Area2D area)
	{
		Parameters.state = Parameters.state == 0 ? 1 : Parameters.state;
		deathCount++;
		
		Kill();
		fitness = Math.Abs(first.Position.x + best - Position.x);
		if(distance > -200)
			fitness += 150;
		
		entered = true;
		Genetic.osobniki[index - 1].fitness = fitness;
		
	
	}
	public void Kill()
	{
		var sprite = GetNode("AnimatedSprite") as AnimatedSprite;
		sprite.Frame = 1;
		gravity = 100000;
		var obstacle1 = (Obstacle)first;
		var obstacle2 = (Obstacle)second;
		obstacle1.SetSpeed(0f);
		obstacle2.SetSpeed(0f);
	}
	public override void _PhysicsProcess(float delta)
	{
		motion.y += (float)gravity;
		if (distance <= whenJump && IsOnFloor() && (check == false))
		{
			motion.y -= (float)jumpForce;
			check = true;
		}
		if ((distance < 0) && (distance2 > 0) && IsOnFloor())
		{
			motion.y -= (float)jumpForce;
			if(!entered){
			fitness = Math.Abs(first.Position.x + best - Position.x);
			Genetic.osobniki[index - 1].fitness = fitness;
			}
			//GD.Print(fitness);
		}
		motion = MoveAndSlide(motion, new Vector2(0, -1));
		/* KONIEC GRY */
		if ( index == generationCount)
		{
			Parameters.state = deathCount == 0 ? 2 : 1;
			
			if (Parameters.maxiter <= generation)
			{
				Parameters.state = deathCount == 0 ? 2 : 3;
				
			}
			
		}
	}
	private void Init()
	{
		Genetic.osobniki = null;

		generation = 1;
		index = 0;
		deathCount = 0;
		srednia = "";
		Parameters.state = 0;
		Genetic.fitnessy = new List<float>();
		Genetic.deadcounty = new List<int>();
	}
}
