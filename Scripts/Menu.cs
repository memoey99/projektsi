using Godot;
using System;

public class Menu : Node2D
{
	Slider popSizeSlider;
	Slider maxiterSlider;
	Slider pcSlider;
	Slider pmSlider;
	Slider elitismSlider;
	Label popSizeLabel;
	Label maxiterLabel;
	Label pcLabel;
	Label pmLabel;
	Label elitismLabel;

	public override void _Ready()
	{
		popSizeSlider = GetNode("/root/Menu/PopSize") as Slider;
		maxiterSlider = GetNode("/root/Menu/Maxiter") as Slider;
		pcSlider = GetNode("/root/Menu/PCrossover") as Slider;
		pmSlider = GetNode("/root/Menu/PMutation") as Slider;
		elitismSlider = GetNode("/root/Menu/Elitism") as Slider;
		popSizeLabel = GetNode("/root/Menu/PopSize/PopSizeValue") as Label;
		maxiterLabel = GetNode("/root/Menu/Maxiter/MaxiterValue") as Label;
		pcLabel = GetNode("/root/Menu/PCrossover/PCValue") as Label;
		pmLabel = GetNode("/root/Menu/PMutation/PMValue") as Label;
		elitismLabel = GetNode("/root/Menu/Elitism/ElitismValue") as Label;
		Parameters.gravity = 20.0;
		Parameters.speed = 300f;
	}

	public override void _Process(float delta)
	{
		if (Input.IsActionJustPressed("exit"))
		{
			GetTree().Quit();
		}
		popSizeLabel.Text = popSizeSlider.Value.ToString();
		maxiterLabel.Text = maxiterSlider.Value.ToString();
		pcLabel.Text = pcSlider.Value.ToString();
		pmLabel.Text = pmSlider.Value.ToString();
		elitismLabel.Text = elitismSlider.Value.ToString();
		elitismSlider.MaxValue = popSizeSlider.Value;
		
	}
	public void OnButtonPressed()
	{
		Parameters.popSize = Convert.ToInt32(popSizeSlider.Value);
		Parameters.maxiter = Convert.ToInt32(maxiterSlider.Value);
		Parameters.pCrossover = pcSlider.Value;
		Parameters.pMutation = pmSlider.Value;
		Parameters.elitism = Convert.ToInt32(elitismSlider.Value);
		GD.Print(Parameters.PrintParameters());
		GetTree().ChangeScene("res://Scenes/Game.tscn");
	}
	public void OnCreditsButtonPressed()
	{
		GetTree().ChangeScene("res://Scenes/Credits.tscn");
	}

	public void OnNormalSpeedButtonPressed()
	{
		Parameters.speed = 300.0f;
		Parameters.gravity = 20.0f;
	}
	public void OnDoubleSpeedButtonPressed()
	{
		Parameters.speed = 600.0f;
		Parameters.gravity = 40.0f;
	}
	public void OnTripleSpeedButtonPressed()
	{
		Parameters.speed = 900.0f;
		Parameters.gravity = 60.0f;
	}
}
