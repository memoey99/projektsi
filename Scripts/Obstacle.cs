using Godot;
using System;

public class Obstacle : StaticBody2D
{
	[Export] float moveSpeed = 50.0f;
	Vector2 motion = new Vector2();
	public override void _Ready()
	{
		moveSpeed = Parameters.speed;
	}
	public override void _PhysicsProcess(float delta)
	{
		float move = Position.x;
		move -= moveSpeed * delta;

		Position = new Vector2(move, Position.y);
	}
	public void SetSpeed(float speed) { moveSpeed = speed; }
}
