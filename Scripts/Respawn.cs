using Godot;
using System;

public class Respawn : Area2D
{
	[Export] float moveSpeed = 70.0f;
	public override void _Ready()
	{
		moveSpeed = Parameters.speed;
	}
	public void OnEntered(Area2D area)
	{
		if (Parameters.state < 2)
		{
			GetTree().ReloadCurrentScene();
			Parameters.state = 0;
			return;
		}
	   
		Genetic.fitnessy.Add(float.Parse(Genetic.Srednia()));
		Genetic.deadcounty.Add(Player.deathCount);
		Genetic.writeToFile(Player.generation, Player.deathCount, float.Parse(Genetic.Srednia()));
		moveSpeed = 0.0f;
		var winnerLabel = GetNode("/root/Game/WinnerLabel") as Label;
		winnerLabel.Visible = true;

		winnerLabel.Text = Parameters.state == 2 ? "WINNER" : "LOOOSER";
	}
	public override void _PhysicsProcess(float delta)
	{
		float move = Position.x;
		move -= moveSpeed * delta;

		Position = new Vector2(move, Position.y);
	}
}
