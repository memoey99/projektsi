public static class Parameters
{
	public static int popSize;
	public static int maxiter;
	public static double pCrossover;
	public static double pMutation;
	public static int elitism;
	public static int minJumpForce;
	public static int maxJumpForce;
	public static int minWhenJump;
	public static int maxWhenJump;
	public static int state = 0; // 0 - zywy, 1 - nie zyje, 2 - koniec gry win, 3 - przegrana
	public static double gravity = 20.0;
	public static float speed = 300f;
	public static string PrintParameters()
	{
		return ("PS = " + popSize + "\nMItr = " + maxiter +
		"\npc = " + pCrossover + "\npm = " + pMutation + "\nE = " + elitism);
	}
}
